# Restaurant Sentiment Analysis
This is a model for restaurant sentiment analysis using GRU.

This project is using `Python Ver 3.6.5`

## Install dependencies
`pip install -r requirements.txt`

## Functions

### Preprocessing
`def Preprocessing(list)`

* ####  Description
    Clean dataset

*  #### Parameters
    list - List of sentences to preprocess

*  #### What the fuction returns
    Returns list of clean dataset

### Get_Model
`def get_model(X, Y, dropout)`

* ####  Description
    GRU Model

*  #### Parameters
    X - X train
    
    Y - Y train
    
    dropout - the value of dropout used

*  #### What the fuction returns
    Returns GRU model ready to fit

### Evaluate
`def evaluate(model, X, Y)`

* ####  Description
    Evaluate GRU model

*  #### Parameters
    model - GRU model to evaluate
    
    X - X test
    
    Y - Y test

*  #### What the fuction returns
    Returns Accuracy, F1 Score, Recall, and Precision

### Confusion Matrix
`def conf_matrix(model, X, Y)`

* ####  Description
    Evaluate model using heatmap

*  #### Parameters
    model - GRU model to evaluate
    
    X - X test
    
    Y - Y test

*  #### What the fuction returns
    Shows heatmap of predicted sentiment and the true sentiment

### Predict Demonstration
`def predict_sentiment(list)`

* ####  Description
    To predict sentiment of each sentences

*  #### Parameters
    list - List of sentences ready to predict

*  #### What the fuction returns
    Returns sentiment of each sentences